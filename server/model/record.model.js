const { DataTypes } = require("sequelize");
const db = require("../db");

const Record = db.define("records", {
    name:DataTypes.TEXT,
    filePath:DataTypes.TEXT,
    rawTranscription:DataTypes.TEXT,
    finalTranscription:DataTypes.TEXT,
 });

module.exports = Record;