
const wav = require('wav');
const fs = require('fs');
const { SpeechClient } = require('@google-cloud/speech'); //Google Cloud Speech to Text Library
const client = new SpeechClient();
const moment = require('moment');
const io = require('socket.io');
const db = require('./db');
const Record = require('./model/record.model');
const { Sequelize } = require('sequelize');


const startAsrServer = async(server)=>{
    
    try{fs.mkdirSync('public/recorded')
    }catch(e){}
    const ioServer = io(server);
    const request = {
        config: {
            encoding: "LINEAR16",
            sampleRateHertz: 16000,
            languageCode: "en-US",
            audioChannelCount: 1,
            enableAutomaticPunctuation: true,
            model:'video'
        },

        interimResults: true,
    };
    ioServer.on('connection', (socket) => {
        let recognizeStream = null;
        let outputStream = null;
        let wavWriter = null;
        let filePath = '';
        let recordName = '';
        let record = null;
        let startTimestamp = 0;
        let lastTranscription = '';
        const sendListRecord = async ()=>{
            list = await Record.findAll({order:[
                ['id','DESC']
            ]}) 
            socket.emit('listRecordReceived',list)
        }
        sendListRecord();
        socket.on('start', async(data) => {
            startTimestamp = new Date().getTime() / 1000;
            socket.emit('recordingStarted')
            wavWriter = new wav.Writer({
                sampleRate: 16000,
                channels: 1,
            });
            recordName = `record-${moment().format('YYYY-MM-DD hh.mm.ss')}`;
            filePath = `recorded/${recordName}`;
            outputStream = fs.createWriteStream('public/'+filePath);
            
            record = await Record.create({
                name:recordName,
                filePath:filePath+'.wav',
                rawTranscription:'',
                finalTranscription:'',
            })
            sendListRecord();
            socket.emit('activeRecordReceived',record);
            const updateTranscription =async (transcription)=>{
                if(transcription !=lastTranscription){
                    record.update({
                        rawTranscription : Sequelize.fn('CONCAT', Sequelize.col("rawTranscription"),transcription+'\r\n'),
                        finalTranscription : Sequelize.fn('CONCAT', Sequelize.col("finalTranscription"),transcription+'\r\n'),
                    }) 
                }
                lastTranscription = transcription;
            }
            // Create Reqcognition Stream
            recognizeStream = client
            .streamingRecognize(request)
            .on('error', console.error)
            .on('data', async(response) => {
                const result = response.results[0];
                const transcription = result.alternatives[0].transcript;
                const seconds = (new Date().getTime() / 1000) - startTimestamp
                timestamp = new Date(seconds * 1000).toISOString().substring(11, 19)
                if(result.isFinal){
                    await updateTranscription(`[${timestamp}] ${transcription}`)
                }
                socket.emit('transcription', {
                    isFinal:result.isFinal,
                    transcription:transcription,
                    timestamp:timestamp
                });
            });
        
        });
    
        socket.on('audio', (data) => {
            if(outputStream) outputStream.write(data);
            if(recognizeStream) recognizeStream.write(data);
        });
    
        socket.on('stop', async (data) => {
            socket.emit('message','Ended')
            if(recognizeStream){
                recognizeStream.end();
                recognizeStream = null;
            } 
            if(outputStream){
                const writer = new wav.Writer({
                    sampleRate:16000,
                    channels:1,
                    bitDepth:16,
                });
                const output = fs.createWriteStream(`public/${filePath}.wav`);
                writer.pipe(output)
        
                const input = fs.createReadStream(`public/${filePath}`);
                input.on('data', (chunk) => {
                    writer.write(chunk);
                });
                input.on('end', async () => {
                    writer.end();
                    fs.unlinkSync(`public/${filePath}`);
                    sendListRecord();
                    await record.reload();
                    socket.emit('finalRecordReceived',record);
                });
                outputStream.close();
            } 
        });

        socket.on('listRecord',(data)=>{
            sendListRecord()
        })
    });
}

module.exports={
    startAsrServer
}